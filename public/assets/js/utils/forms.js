(function() {
  'use strict';
  define(function() {
    return {
      checkCaptcha: function(current_form) {
        var captcha, captcha_valid, captcha_wrapper, error;
        captcha_valid = null;
        captcha = current_form.find(".g-recaptcha");
        if (captcha.length > 0) {
          captcha_wrapper = captcha.parents(".form-group");
          captcha_wrapper.removeClass("has-error").find(".error").remove();
          if (grecaptcha.getResponse() === "") {
            error = '<span class="error">' + captcha.attr("data-required-msg") + '</span>';
            captcha_wrapper.addClass("has-error").append(error);
            return false;
          }
        }
      },
      validateCommonForm: function(current_form, options) {
        var _this, defaults, settings;
        if (options === null) {
          options = "";
        }
        _this = this;
        current_form.on("submit", function(e) {
          _this.checkCaptcha(current_form);
          window.go_to_first_error = false;
          if (!current_form.hasClass("payment-method")) {
            current_form.find(".form-msg").empty().removeClass("hide success error");
            current_form.find(".form-control").blur();
            current_form.find(".has-error").removeClass("has-error").find(".error").remove();
            return current_form.find(".form-msg").addClass("hide").find(".alert").removeClass("alert-error alert-success").find(".msg").html("&nbsp;");
          }
        });
        defaults = {
          errorElement: "b",
          rules: "",
          messages: "",
          onkeyup: false,
          focusInvalid: false,
          onfocusout: false,
          onclick: false,
          errorClass: "error",
          highlight: function(element) {
            $(element).parents(".form-group").removeClass("valid");
            return $(element).parents(".form-group").addClass("has-error");
          },
          unhighlight: function(element) {
            return $(element).parents(".form-group").removeClass("has-error");
          },
          errorPlacement: function(error, element) {
            if (element.parents(".checkbox, .radio").length > 0) {
              return error.css("display", "block").appendTo(element.parents(".checkbox, .radio"));
            } else {
              return error.css("display", "block").appendTo(element.parents(".form-group"));
            }
          },
          submitHandler: function(form) {
            return form.submit();
          }
        };
        settings = $.extend({}, defaults, options);
        return require(["validate"], function() {
          $.validator.addMethod("filesize", function(value, element, param) {
            return this.optional(element) || element.files[0].size <= param;
          });
          return current_form.validate(settings);
        });
      },
      getRequiredField: function(selected_value, values, current_value, _this) {
        var form, obj, selector;
        if (values) {
          obj = eval("(" + values + ")");
          selector = obj["elements"][selected_value];
          if (obj["multiple"] !== "true") {
            $(obj["none"]).hide().addClass("hide");
          }
          if (current_value === "") {
            return $(obj["none"]).hide().addClass("hide");
          } else {
            form = $(_this).parent().closest("form");
            if (obj["checkAllOptions"] === "true") {
              return form.find(obj["elements"]["true"]).show().removeClass("hide");
            } else {
              if ($(selector).length > 0) {
                return form.find(selector).show().removeClass("hide");
              } else {
                if (obj["multiple"] === "true") {
                  return $(selector).hide().addClass("hide");
                }
              }
            }
          }
        }
      },
      setDatepicker: function() {
        var datepicker, datepicker_opts;
        datepicker = ".datepicker";
        datepicker_opts = {
          minDate: 0,
          dateFormat: "dd/mm/yy",
          duration: 250
        };
        return require(["jqueryUI", "datepickerEs"], function() {
          return $(datepicker).datepicker(datepicker_opts).focus(function() {
            return $(this).next("i").addClass("datepicker-opened");
          }).blur(function() {
            var _el;
            _el = this;
            return setTimeout((function() {
              return $(_el).next("i").removeClass("datepicker-opened");
            }), 300);
          }).next("i").click(function() {
            if (!$(this).hasClass("datepicker-opened")) {
              $(this).prev().datepicker("show");
              return $(this).addClass("datepicker-opened");
            } else {
              return $(datepicker).datepicker("hide");
            }
          });
        });
      },
      placeHolderForm: function() {
        return $("input, textarea").not("input[type=submit]").each(function() {
          var currentValue;
          currentValue = $(this).val();
          $(this).focus(function() {
            if ($(this).val() === currentValue) {
              return $(this).val("");
            }
          });
          return $(this).blur(function() {
            if ($(this).val() === "") {
              return $(this).val(currentValue);
            }
          });
        });
      },
      checkEmpty: function(obj) {
        if (obj.val()) {
          return obj.addClass("filled");
        } else {
          return obj.removeClass("filled");
        }
      },
      showFormMessage: function(selector, type, info) {
        var msg_markup;
        msg_markup = '<div class="alert has-close has-icon"><button type="button" class="close"><i class="fa fa-times"></i></button><div class="icon"></div><div class="info msg"></div></div>';
        selector.empty();
        return selector.removeClass("hide").html(msg_markup).find(".alert").removeClass("alert-error alert-success").addClass(type).find(".msg").text(info);
      },
      resetForm: function(form) {
        form.find("input, input:file, select, textarea").removeClass("filled").val("");
        return form.find("input:radio, input:checkbox").removeAttr("checked").removeClass("filled").removeAttr("selected");
      },
      disableFormSend: function(form, status, text_status) {
        if (text_status === null) {
          text_status = 'Enviar';
        }
        return form.find("button.btn").prop("disabled", status).html(text_status);
      },
      generalEvents: function() {
        var _this;
        _this = this;
        require(["placeholder"], function() {
          return $("input, textarea").placeholder();
        });
        require(["fileStyle"], function() {
          $(":file").customFileInput();
          return $(".has-icon .customfile").append('<i class="fa fa-cloud-upload"></i>');
        });
        if ($(".datepicker").length > 0) {
          require(["../utils/forms"], function(forms) {
            return forms.setDatepicker();
          });
        }
        $("select.dependent").on("change", function() {
          return _this.getRequiredField($(this).val(), $(this).attr("data-required"), $(this).val(), this);
        }).each(function() {
          return _this.getRequiredField($(this).val(), $(this).attr("data-required"), $(this).val(), this);
        });
        return $("input.dependent").on("click", function(e) {
          var selected_val;
          if ($(this).is(":checked")) {
            selected_val = $(this).val();
          } else {
            selected_val = "";
          }
          return _this.getRequiredField($(this).val(), $(this).attr("data-required"), selected_val, this);
        }).each(function() {
          var selected_val;
          if ($(this).is(":checked")) {
            selected_val = $(this).val();
          } else {
            selected_val = "";
          }
          if (selected_val) {
            return _this.getRequiredField($(this).val(), $(this).attr("data-required"), selected_val, this);
          }
        });
      }
    };
  });

}).call(this);
