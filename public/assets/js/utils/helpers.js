(function() {
  define(function() {
    return {
      logs: function() {
        if ($("body").attr("data-env") !== "development" || !window.console) {
          return window.console = {
            log: function() {}
          };
        }
      },
      goToSection: function(section, offset, time) {
        if (offset == null) {
          offset = 60;
        }
        if (time == null) {
          time = 1000;
        }
        $("html, body").bind("scroll mousedown DOMMouseScroll mousewheel", function() {
          return $("html, body").stop();
        });
        return $("html:not(:animated), body:not(:animated)").stop().animate({
          scrollTop: section.offset().top - offset
        }, time, function() {
          return $("html, body").unbind("scroll mousedown DOMMouseScroll mousewheel");
        });
      },
      trackEventGA: function(category, action, label) {
        return ga("send", "event", category, action, label);
      },
      commonEvents: function() {
        var _this;
        _this = this;
        $(".submit-on-change").on("change", function() {
          $(".submit-on-change").blur();
          return $(this).parents("form").submit();
        });
        $(".reload-page").on("click", function() {
          return location.reload(true);
        });
        $(".print").on("click", function() {
          return window.print();
        });
        $(".open-window").on("click", function() {
          return window.open($(this).attr("data-url"), "openedWindow", "width=" + ($(this).attr("data-width")) + ", height=" + ($(this).attr("data-height")));
        });
        $(document).on("click", ".alert .close", function() {
          return $(this).parents(".alert").remove();
        });
        if ($("form").length > 0) {
          require(["../utils/forms"], function(form) {
            return form.generalEvents();
          });
        }
        $(".btn-menu").on("click", function() {
          var wh, window_ht;
          if (!$(this).hasClass("opened")) {
            window_ht = window.innerHeight ? window.innerHeight : $(window).height();
            wh = window_ht - $("#header .container").outerHeight();
            $(this).addClass("opened");
            $(".top-opts").scrollTop(0).addClass("active");
            if ($(window).width() < 1200) {
              if (wh <= $(".top-opts .items").height()) {
                return $(".top-opts").height(wh);
              } else {
                return $(".top-opts").height($(".top-opts .items").outerHeight());
              }
            }
          } else {
            $(this).removeClass("opened");
            $(".top-opts").removeClass("active");
            return $(".top-opts").height(0);
          }
        });
        $(".go-to").on("click", function() {
          var section;
          section = $(this).attr("data-section");
          return _this.goToSection($(section), 0);
        });
        $(document).on("click", ".track-ga-event", function() {
          return _this.trackEventGA($(this).attr("data-category"), $(this).attr("data-action"), $(this).attr("data-label"));
        });
        if ($(".show-lb-content").length > 0 || $(".show-lb-image").length > 0) {
          return require(["fancybox"], function() {
            $(".open-lb-image").on("click", function(e) {
              return $(".open-lb-image").fancybox({
                padding: 2,
                maxWidth: "95%",
                maxHeight: "95%",
                helpers: {
                  title: {
                    type: "inside"
                  }
                },
                afterClose: function() {
                  return $(".promo-banner").show();
                }
              });
            });
            return $(".show-lb-content").fancybox({
              maxWidth: 800,
              maxHeight: 600,
              fitToView: false,
              width: "80%",
              height: "80%",
              autoSize: false,
              closeClick: false,
              openEffect: "none",
              closeEffect: "none"
            });
          });
        }
      },
      initSliderSwipe: function(obj) {
        return obj.swipe({
          allowPageScroll: "vertical",
          excludedElements: null,
          threshold: 10,
          swipe: function(event, direction) {
            if ($("html.touch").length > 0) {
              if (direction === "left") {
                obj.cycle("next");
              }
              if (direction === "right") {
                return obj.cycle("prev");
              }
            }
          }
        });
      },
      pageSliders: function(responsive) {
        var _this;
        if (responsive == null) {
          responsive = false;
        }
        _this = this;
        require(["cycle", "touchSwipe"], function() {
          $(".benefits-slider").not(".initialized").each(function(index) {
            var num_visible, offset, slider, slider_opts;
            slider = $(this).find(".slides");
            slider.removeClass("visible");
            slider.cycle("destroy");
            num_visible = parseInt($(window).width() / (slider.find(".slide").outerWidth() + 30));
            offset = ($(window).width() - (num_visible * (slider.find(".slide").outerWidth() + 30))) / 2;
            slider_opts = {
              fx: "carousel",
              carouselVisible: 5,
              carouselFluid: true,
              slides: "> div",
              pager: $(this).find(".controls"),
              speed: 1000,
              carouselOffset: offset,
              timeout: 0,
              log: false
            };
            if ($("html.touch").length > 0) {
              slider_opts.speed = 250;
              _this.initSliderSwipe(slider);
            }
            slider.cycle(slider_opts);
            return setTimeout(function() {
              slider.addClass("visible").find(".slide").css("visibility", "visible");
              slider.find(".slide").outerHeight(slider.find(".cycle-carousel-wrap").height());
              return require(["waypoints"], function() {
                return Waypoint.refreshAll();
              });
            }, 35);
          });
          return $(".testimonials-slider").not(".initialized").each(function(index) {
            var slider, slider_opts;
            slider = $(this).find(".slides");
            slider.removeClass("visible");
            slider_opts = {
              fx: "carousel",
              carouselFluid: true,
              carouselVisible: 1,
              slides: "> div",
              pager: $(this).find(".controls"),
              speed: 1000,
              timeout: 0,
              log: false
            };
            if ($("html.touch").length > 0) {
              slider_opts.speed = 250;
              _this.initSliderSwipe(slider);
            }
            slider.cycle(slider_opts);
            $(this).addClass("initialized");
            return setTimeout(function() {
              slider.css("visibility", "visible");
              return require(["waypoints"], function() {
                return Waypoint.refreshAll();
              });
            }, 40);
          });
        });
        return $(window).trigger("resize");
      },
      initHelpers: function() {
        this.logs();
        this.commonEvents();
        return this.generalEvents();
      },
      generalResize: function(cb) {
        return $(window).smartresize(function() {
          if ($(window).width() !== window.window_wt || (Math.abs($(window).height() - window.window_ht) >= 100)) {
            window.window_wt = $(window).width();
            window.window_ht = window.innerHeight ? window.innerHeight : $(window).height();
            return cb();
          }
        });
      },
      generalEvents: function() {
        var _this, html, resizeEvents, section;
        _this = this;
        section = parseInt($("[data-menu-section]").attr("data-menu-section")) - 1;
        $(".menu a").eq(section).addClass("current");
        $(".menu a").click(function() {
          if ($(window).width() < 992) {
            return $(".btn-menu").trigger("click");
          }
        });
        require(["waypoints"], function() {
          var waypoints;
          waypoints = $(".page-content .top-info").waypoint({
            handler: function() {
              return $(".menu a").removeClass("current");
            },
            offset: "-50%"
          });
          waypoints = $(".page-content > section").waypoint({
            handler: function(direction) {
              if (direction === "down") {
                section = $(this.element).attr("class").split(" ");
                section = section[section.length - 1];
                $(".menu a").removeClass("current");
                return $('.menu a[data-section=".' + section + '"]').addClass("current");
              }
            },
            offset: 100
          });
          return waypoints = $(".page-content > section").waypoint({
            handler: function(direction) {
              if (direction === "up") {
                section = $(this.element).attr("class").split(" ");
                section = section[section.length - 1];
                $(".menu a").removeClass("current");
                return $('.menu a[data-section=".' + section + '"]').addClass("current");
              }
            },
            offset: -100
          });
        });
        require(["fancybox"], function() {
          var closeLightbox;
          closeLightbox = function() {
            if ($.fancybox.isOpen) {
              return $.fancybox.close();
            }
          };
          return _this.generalResize(closeLightbox);
        });
        window.window_wt = $(window).width();
        window.window_ht = window.innerHeight ? window.innerHeight : $(window).height();
        resizeEvents = function() {
          var resizeSubmenu;
          $(".top-info").outerHeight(window.window_ht);
          (resizeSubmenu = function() {
            var wh;
            wh = $(window).height() - $("#header .container").outerHeight();
            if ($(window).width() < 1200) {
              if (wh <= $(".top-opts .items").height()) {
                return $(".top-opts").height(window_ht - $("#header .container").outerHeight());
              } else {
                return $(".top-opts").height($(".top-opts .items").outerHeight());
              }
            }
          })();
          return _this.pageSliders();
        };
        this.generalResize(resizeEvents);
        $(".top-info").outerHeight(window.window_ht);
        if ($(".slider").length > 0) {
          this.pageSliders();
        }
        if ("backgroundBlendMode" in document.body.style) {
          html = document.getElementsByTagName("html")[0];
          html.className = html.className + " background-blend-mode";
        }
		
		$(".btn-menu").click(function(){
		  $(".top-menu").addClass("showed");
		  $("html, body").addClass("scroll-lock");
		});
		$(".btn-close-menu").click(function() {
			$(".top-menu").addClass("closing");
		  	setTimeout(function() {
				$(".top-menu").removeClass("showed closing");
				$("html, body").removeClass("scroll-lock");
		  	}, 400);
		});

    document.addEventListener('keyup', function(e){
      if (e.keyCode === 27) {
        if(document.querySelectorAll(".top-menu.showed").length > 0){
          $(".btn-close-menu").trigger('click');
        }
      }
    })
        
        if ($(".newsletter-form").length > 0) {
          return require(["../newsletter"], function(newsletter) {
            return newsletter.initSection();
          });
        }
      }
    };
  });

}).call(this);
