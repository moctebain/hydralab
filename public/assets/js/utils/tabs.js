(function() {
  define(function() {
    return {
      hashTab: function() {
        var _this;
        _this = this;
        return require(["hashChange"], function() {
          $(window).hashchange(function() {
            var hash, section_id;
            hash = window.location.hash.replace("#", "");
            if (hash && $("[href*='#" + hash + "']").length > 0) {
              if (!window.clickedTab) {
                section_id = $("[href*='#" + hash + "']").attr("data-tab-id");
                $("[href*='#" + hash + "']").trigger("click");
                _this.triggerTabs($(".tabs-wrapper").find(".tabs-wrapper"));
              }
              if (window.scrollToBlock) {
                return require(["../utils/helpers"], function(el) {
                  el.goToSection($(".tabs-nav"), 30);
                  return window.scrollToBlock = false;
                });
              }
            }
          });
          return $(window).hashchange();
        });
      },
      toggleTab: function(tab, tab_link, closeCurrent) {
        if (!$(".responsive-tabs").is(":visible")) {
          tab.parents(".tabs-wrapper").first().find(".tab").removeClass("active");
        }
        if (tab.is(":visible") && closeCurrent === true) {
          return tab.removeClass("active").hide();
        } else {
          tab.parents(".tabs-wrapper").first().find(".tab[data-tab-id='" + tab_link + "']").addClass("active");
          tab.parents(".tabs-wrapper").first().find(".tabs-content").first().find("> div").hide().removeClass("active");
          return tab.addClass("active").show(0, function() {
            if (tab.attr("data-tab-cb-file") || tab.attr("data-tab-cb")) {
              return require(["../utils/" + tab.attr("data-tab-cb-file")], function(el) {
                return el[tab.attr("data-tab-cb")]();
              });
            }
          });
        }
      },
      checkTab: function(obj, tab_id) {
        if ($.isNumeric(tab_id)) {
          return this.toggleTab(obj.parents(".tabs-wrapper").first().find(".tabs-content > div").eq(tab_id), tab_id, false);
        } else {
          return this.toggleTab(obj.parents(".tabs-wrapper").find(tab_id), tab_id, false);
        }
      },
      triggerTabs: function(selector) {
        return selector.each(function() {
          var init_tab;
          init_tab = $(this).attr("data-init-tab") || 0;
          window.last_opened_tab = init_tab;
          if (!(init_tab < 0)) {
            return $(this).children(".tabs-nav").find(".tab").eq(init_tab).trigger("click");
          }
        });
      },
      navTabsHandler: function() {
        var _this;
        _this = this;
        $(".tabs-nav .tab").on("click", function(e) {
          var tab;
          window.clickedTab = true;
          tab = $(this).attr("data-tab-id");
          window.last_opened_tab = tab;
          if ($.isNumeric(parseInt(tab))) {
            return _this.checkTab($(this), parseInt(tab));
          } else {
            return _this.checkTab($(this), tab);
          }
        });
        return $(".responsive-tabs .tab").on("click", function() {
          var tab_id;
          tab_id = $(this).attr("data-tab-id");
          if (!$(this).hasClass("active")) {
            window.last_opened_tab = tab_id;
            return $(this).parents(".tabs-wrapper").first().find(".tabs-nav .tab[data-tab-id='" + tab_id + "']").trigger("click");
          } else {
            window.last_opened_tab = $(".responsive-tabs .tab.active").attr("data-tab-id");
            return $(this).removeClass("active");
          }
        });
      },
      initTabs: function() {
        var _this, check_url, hash;
        _this = this;
        check_url = false;
        window.last_opened_tab = null;
        hash = check_url ? window.location.hash.replace("#", "") : false;
        $(".tabs-wrapper .tabs-content .tab-pane").each(function(index) {
          return $(this).parents(".tabs-wrapper").find(".responsive-tabs > ul > li").eq(index).append('<div class="info">' + $(this).html() + '</div>');
        });
        window.clickedTab = false;
        this.navTabsHandler();
        if (hash && $("[href*='#" + hash + "']").length > 0) {
          window.scrollToBlock = true;
        } else {
          this.triggerTabs($(".tabs-wrapper"));
        }
        if (check_url) {
          return this.hashTab();
        }
      }
    };
  });

}).call(this);
