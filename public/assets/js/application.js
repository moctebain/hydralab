
/*
* application.js
 */

(function() {
  var csrfSafeMethod, env, getCookie;

  if (!Object.keys) {
    Object.keys = function(o) {
      var k, p;
      if (o !== Object(o)) {
        throw new TypeError('Object.keys called on a non-object');
      }
      k = [];
      p = void 0;
      for (p in o) {
        continue;
      }
      return k;
    };
  }

  getCookie = function(name) {
    var cookie, cookieValue, cookies, i;
    cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      cookies = document.cookie.split(';');
      i = 0;
      while (i < cookies.length) {
        cookie = jQuery.trim(cookies[i]);
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
        i++;
      }
    }
    return cookieValue;
  };

  csrfSafeMethod = function(method) {
    return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
  };

  env = document.querySelector('body').getAttribute('data-env');

  window.assets_base_url = env === 'development' ? '/assets' : '/assets';

  require.config({
    baseUrl: assets_base_url + '/js/vendor/',
    paths: {
      async: 'async',
      modernizr: 'modernizr',
      jquery: 'jquery-1.11.1.min',
      jqueryUIWidget: 'jquery.ui.widget',
      iframeTransport: 'jquery.iframe-transport',
      fileUpload: 'jquery.fileupload',
      validate: 'jquery.validate',
      placeholder: 'jquery.placeholder.min',
      waypoints: 'jquery.waypoints.min',
      fancybox: 'jquery.fancybox',
      cycle: 'jquery.cycle2',
      touchSwipe: 'jquery.touchSwipe.min',
      fileStyle: 'jquery-filestyle'
    },
    shim: {
      fileUpload: ['jquery'],
      validate: ['jquery'],
      placeholder: ['jquery'],
      waypoints: ['jquery'],
      fancybox: ['jquery'],
      cycle: ['jquery'],
      touchSwipe: ['jquery']
    },
    urlArgs: 'version=' + (new Date()).getTime()
  });

  require(['jquery'], function($) {
    (function($, sr) {
      var debounce;
      debounce = function(func, threshold, execAsap) {
        var timeout;
        timeout = void 0;
        return function() {
          var args, delayed, obj;
          obj = this;
          args = arguments;
          delayed = function() {
            if (!execAsap) {
              func.apply(obj, args);
            }
            timeout = null;
          };
          if (timeout) {
            clearTimeout(timeout);
          } else if (execAsap) {
            func.apply(obj, args);
          }
          timeout = setTimeout(delayed, threshold || 100);
        };
      };
      jQuery.fn[sr] = function(fn) {
        if (fn) {
          return this.bind('resize', debounce(fn));
        } else {
          return this.trigger(sr);
        }
      };
    })(jQuery, 'smartresize');
    $.fn.serializeObject = function() {
      var a, o;
      o = {};
      a = this.serializeArray();
      $.each(a, function() {
        if (o[this.name] !== void 0) {
          if (!o[this.name].push) {
            o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
        } else {
          o[this.name] = this.value || '';
        }
      });
      return o;
    };
    return $(function() {
      var Project;
      Project = {
        init: function() {
          var csrftoken;
          csrftoken = getCookie('csrftoken');
          $.ajaxSetup({
            beforeSend: function(xhr, settings) {
              if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                return xhr.setRequestHeader('X-CSRFToken', csrftoken);
              }
            }
          });
          $('body').attr('data-assets-base-url', window.assets_base_url);
          return Project.initSections();
        },
        initSections: function() {
          return require(['../utils/helpers'], function(helpers) {
            return helpers.initHelpers();
          });
        }
      };
      return Project.init();
    });
  });

}).call(this);
