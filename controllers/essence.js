'use strict';
var _ = require('underscore');

// List 
exports.list = function(req, res){
	
	// Info
	var members = [
		{
			name: 'Alejandra',
			filename: 'alejandra',
			role: res.__('essence.members.roles.alejandra_role')
		},
		{
			name: 'Alfonso',
			filename: 'alfonso',
			role: res.__('essence.members.roles.alfonso_role')
		},
		{
			name: 'Alonso',
			filename: 'alonso',
			role: res.__('essence.members.roles.alonso_role')
		},
		{
			name: 'Angel',
			filename: 'angel',
			role: res.__('essence.members.roles.angel_role')
		},
		{
			name: 'Cecilia',
			filename: 'cecilia',
			role: res.__('essence.members.roles.cecilia_role')
		},
		{
			name: 'Claudio',
			filename: 'claudio',
			role: res.__('essence.members.roles.claudio_role')
		},
		{
			name: 'Esteban',
			filename: 'esteban',
			role: res.__('essence.members.roles.esteban_role')
		},
		{
			name: 'Gabriela',
			filename: 'gaby',
			role: res.__('essence.members.roles.gaby_role')
		},
		{
			name: 'Ismael',
			filename: 'ismael',
			role: res.__('essence.members.roles.ismael_role')
		},
		{
			name: 'Jessica',
			filename: 'jessica',
			role: res.__('essence.members.roles.jessica_role')
		},
		{
			name: 'Karla',
			filename: 'karla',
			role: res.__('essence.members.roles.karla_role')
		},
		{
			name: 'Ricardo',
			filename: 'ricardo',
			role: res.__('essence.members.roles.ricardo_role')
		}
	];

	return _.shuffle(members);
}