'use strict';
var i18n = require('i18n'),
	path = require('path');

// i18n configuration
module.exports.configLocale = function(app){

	i18n.configure({
		locales: ['en', 'es'],
		// cookiename: 'locale',
		defaultLocale: 'en',
		objectNotation: true,
		directory: path.dirname(require.main.filename) + '/locales',
		// autoReload: true,
		// updateFiles: false,
		// syncFiles: false
	});

	// Init locales
	this.initLocale(app);

}

// i18n
module.exports.initLocale = function(app){

	app.use(i18n.init)

}

// Set current locale
module.exports.setLocale = function(req, res){
  
	var locale = i18n.getLocale(),
		url_lang = null;  

  	if(typeof req.url !== 'undefined'){        
		url_lang = req.url.replace(/^\/([^\/]*).*$/, '$1');      
	}

	// Check URL changes
	if (url_lang === 'es' || url_lang === 'en'){
		if(url_lang !== locale){
			locale = url_lang;                
		}
	}

	// Set locale	
	i18n.setLocale(locale);
    res.locals.language = locale;
    // res.cookie('locale', locale,  { maxAge: 900000, httpOnly: true });

    return locale;
}