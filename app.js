'use strict';
var express = require('express'),
    url = require('url'),
    path = require('path'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    init = require('./config/config'),
    config = init.getConfig(),
    locales = require('./lib/locales'),
    app = express(),
    routes,
    PORT = config.port;

// Settings
app.set('port', PORT);
app.locals = {
  env: app.get('env') 
}
app.use(cookieParser());

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', config.views.engine);

// Assets
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// Init i18n support
locales.configLocale(app);

// Locals
app.use(function (req, res, next) {
  res.setLocale(locales.setLocale(req, res));
  app.locals.siteTitle = res.__("site_title");
  next()
});

// Routes
routes = require('./config/routes').routes();
app.use(routes);

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error(req.__('page_not_found'));
  err.status = 404;
  next(err);
});

// Error handlers
// Development error handler will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      pageTitle: err.message,
      page: null,      
      message: err.message,
      error: err
    });
  });
}

// Production error handler
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    pageTitle: err.message,
    page: null,      
    message: err.message,
    error: err
  });
});

// Start server
app.listen(PORT, function(){
  console.log('Listening on port: ' + PORT);
});