'use strict';
var express = require('express'),
	router = express.Router(),
	i18n = require('i18n'),
  _ = require('underscore'),
  pages = require('./pages');

module.exports.routes = function(){
  
  pages.forEach(function(value, i){
    
    // Defaults
    var page = pages[i].name,
        slug = 'general.menu.' + page + '.slug',
        route = (page === "home") ? ['/', '/:locale(es|en)'] : i18n.__l(slug),
        page_info = pages[i].info,
        get_info = (typeof page_info !== "undefined" && typeof page_info.getInfo !== "undefined") ? page_info.getInfo : null,
        common_info = {
          pageTitle: i18n.__('general.menu.' + page + '.name'),
          page: page
        };

    router.get(route, function(req, res){

      // Get info
      if(page_info && get_info){
        common_info[get_info.field] = require(get_info.file)[get_info.fn](req, res);
      }

      req.app.locals.slug = slug;
      res.render(page, _.extend(common_info, page_info));
    });
  })
  router.get('/lang/:locale', function (req, res, next) {    
      var lang = req.params.locale;
      if ((lang === 'en') || (lang === 'es')) {        
        i18n.setLocale(req.params.locale);
        // res.cookie('locale', req.params.locale, { maxAge: 900000, httpOnly: true });
        if (req.headers.referer){
          res.redirect(i18n.__(req.app.locals.slug));
        } else {
          res.redirect('/');
        }
      } else {
        res.redirect('/error');
      }
  });
  
  return router;

}