'use strict';

module.exports.getEnv = function(){	
	return {
		name: process.env.NODE_ENV ? process.env.NODE_ENV : 'development'
  	}
}