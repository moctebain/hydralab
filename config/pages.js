'use strict';
var _ = require('underscore');

module.exports = [
  {
    name: "home"
  },
  {
    name: "essence",
    info: {
      getInfo: {
        file: "../controllers/essence",
        fn: "list",
        field: "members"
      }
    }
  },
  {
    name: "services"
  }
]