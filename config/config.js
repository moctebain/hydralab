'use strict';
var fs = require('fs'),
	path = require('path'),
	yaml = require('js-yaml'),
	env = require('./environment'),
	config = yaml.safeLoad(fs.readFileSync(__dirname + '/application.yml', 'utf-8')),
	init = {};

init = {

	// Get environment
	environment: env.getEnv().name,

	// Get config file
	getConfig: function(){
		return config[this.environment] || {};
	}
}

module.exports = init;